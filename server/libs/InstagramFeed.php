<?php

abstract class InstagramFeed_Service_Base
{


    public function __construct()
    {

    }

    private function _getBaseUri($login)
    {
        return 'https://www.instagram.com/' . $login . '/media/';
    }

    public function sendRequest($dataRequest)
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->_getBaseUri($dataRequest['login']));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($ch);

        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE); #Получим HTTP-код ответа сервера

        $this->handlerEror($code);

        curl_close($ch);

        return $response;
    }

    public function handlerEror($code)
    {
        $code = (int)$code;
        $errors = array(
            301 => 'Moved permanently',
            400 => 'Bad request',
            401 => 'Unauthorized',
            403 => 'Forbidden',
            404 => 'Not found',
            500 => 'Internal server error',
            502 => 'Bad gateway',
            503 => 'Service unavailable'
        );
        try {
            #Если код ответа не равен 200 или 204 - возвращаем сообщение об ошибке
            if ($code != 200 && $code != 204)
                throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error', $code);
        } catch (Exception $E) {
            die('Ошибка: ' . $E->getMessage() . PHP_EOL . 'Код ошибки: ' . $E->getCode());
        }
    }

}

class InstagramFeed_Service extends InstagramFeed_Service_Base
{
    //Вызывается автоматически
    public function __construct()
    {
        //Инициализируем конструктор родителя только вручную
        parent::__construct();

    }

    public function getFeed($login)
    {
        $dataRequest = [
            'login' => $login,
        ];

        $response = $this->sendRequest($dataRequest);

        return $response;

        //Обрабатываем данные
    }


}