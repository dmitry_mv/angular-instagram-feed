(function (angular) {
    'use strict';

    angular.module('app', ['ngRoute', 'ngMaterial', 'instagram.feed'])
        .config(config);

    config.$inject = ['$mdIconProvider'];
    function config($mdIconProvider) {
        var path = 'img/icons/';
        $mdIconProvider

        //header
            .iconSet('favorite:white', path + 'ic_favorite_white_18px.svg', 'auto')
            .iconSet('comments:white', path + 'ic_mode_comment_white_18px.svg', 'auto')

            .iconSet('move_left:white', path + 'ic_keyboard_arrow_left_white_48px.svg', '48')
            .iconSet('move_right:white', path + 'ic_keyboard_arrow_right_white_48px.svg', '48')

            .iconSet('close:white', path + 'ic_close_white_48px.svg', '48')

    }

})(window.angular);