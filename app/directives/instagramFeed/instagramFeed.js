(function (angular) {
    'use strict';

    angular.module('instagram.feed', ['ngRoute'])
        .directive("instagramFeed", directiveFunc);

    directiveFunc.$inject = [];
    function directiveFunc() {
        var directive = {
            restrict: 'EA',
            templateUrl: 'app/directives/instagramFeed/instagramFeed.html',
            scope: {
                login: '@'
            },
            link: linkFunc,
            controller: directiveController,
            controllerAs: 'vm'
        };
        directiveController.$inject = ['$scope', '$http', '$httpParamSerializerJQLike', '$window'];
        return directive;

        function directiveController($scope, $http, $httpParamSerializerJQLike, $window) {

            var vm = this;

            vm.feed = {
                login: $scope.login,
                data: [],
                getAllData: function () {
                    var self = this;
                    return $http({
                        method: 'post',
                        data: $httpParamSerializerJQLike({
                            login: self.login
                        }),
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        url: 'server/instagram-feed.php'
                    })
                        .success(complete)
                        .error(failed);

                    function complete(response) {
                        return response.data;
                    }

                    function failed(error) {
                        console.log('Instagram Feed Directive' + error);
                    }
                },
                list: {
                    dataOriginal: [],
                    data: [],
                    listFeed: [],
                    index: 0,
                    count: 0,
                    pageCount: 6,
                    loadMoreShow: false,
                    rowElements: 3
                },
                groupedFeed: function () {
                    var self = this;

                    var arr = [];
                    var list = [];

                    self.list.dataOriginal = angular.copy(self.data);

                    var indexedCount = 1;
                    self.list.dataOriginal.forEach(function (item) {
                        item.open = false;
                        item.empty = false;
                        item.index = indexedCount;
                        indexedCount++;
                    });
                    self.gallery.lastIndex = indexedCount - 1;

                    var lastElementsCount = self.list.dataOriginal.length % self.list.rowElements;
                    if (lastElementsCount != 0) {
                        var endIndex = self.list.rowElements - lastElementsCount;
                        for (var j = 0; j < endIndex; j++) {
                            self.list.dataOriginal.push({
                                empty: true,
                                open: false
                            });
                        }
                    }

                    self.list.count = self.list.dataOriginal.length;

                    for (var i = 1; i <= self.list.count; i++) {
                        list.push(self.list.dataOriginal[i - 1]);
                        if (i % self.list.rowElements == 0) {
                            arr.push(list);
                            list = [];
                        }
                    }

                    self.list.data = arr;

                },
                gallery: {
                    dateAdded: '',
                    index: 0,
                    lastIndex: 0,
                    image: {},
                    infoWindow: 350,
                    maxH_window: 600,
                    maxW_window: 600,
                    video: {},
                    modalId: 'instagram-gallery',
                    imageSizeId: 'instagram-gallery__wrapper__img',
                    infoSizeId: 'instagram-gallery__wrapper__info-block',
                    wrapperId: 'instagram-gallery__wrapper__container',
                    current: {},
                    spinner: false,
                    open: false,
                    getFormatDate: function (value) {
                        var date = new Date(value * 1000);
                        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                        var year = date.getFullYear();
                        var month = months[date.getMonth()];
                        var newDate = date.getDate();
                        var time = month + ', ' + newDate + ' ' + year;
                        return time;
                    },
                    moveLeft: function () {
                        var self = this;
                        var index = vm.feed.gallery.current.index;
                        if (index > 1) {
                            //1. Найти нужный элемент
                            self.index = index - 1;
                            self.current = vm.feed.list.dataOriginal[self.index - 1];
                            //2. Инициализировать галерею
                            self.loadImageGallery();
                        }
                    },
                    moveRight: function () {
                        var self = this;
                        var index = vm.feed.gallery.current.index;
                        if (index < self.lastIndex) {
                            self.index = index + 1;
                            self.current = vm.feed.list.dataOriginal[index];
                            //2. Инициализируем галерею
                            self.loadImageGallery();
                        }
                    },
                    initMaxWindow: function () {

                        var self = this;
                        self.clientHeight = window.innerHeight
                            || document.documentElement.clientHeight
                            || document.body.clientHeight;
                        self.clientWidth = window.innerWidth
                            || document.documentElement.clientWidth
                            || document.body.clientWidth;

                        self.maxH_window = self.clientHeight - 40 * 2;
                        self.maxW_window = self.clientWidth - 60 * 2;

                    },
                    openGallery: function (item) {
                        var self = this;

                        self.current = item;

                        self.loadImageGallery();

                        self.onOpen();
                    },
                    loadImageGallery: function () {
                        var self = this;

                        self.dateAdded = self.getFormatDate(self.current.created_time);

                        self.initMaxWindow();
                        var item = self.current;
                        self.image = item.images.standard_resolution;
                        var img = new Image();
                        img.src = item.images.standard_resolution.url;
                        img.onload = function () {
                            self.onAlignHeight();
                        };
                    },
                    onOpen: function () {
                        var self = this;
                        document.body.style.overflow = 'hidden';
                        document.getElementById(self.modalId).style.display = 'block';
                        document.getElementById(self.modalId).style.overflowY = 'auto';
                        self.open = true;
                    },
                    onClose: function () {
                        var self = this;
                        document.body.style.overflow = 'auto';
                        document.getElementById(self.modalId).style.display = 'none';
                        self.open = false;
                    },
                    onAlignHeight: function () {
                        var self = this;

                        var maxH_image = self.image.height;
                        var maxW_image = self.image.width;
                        //1. Принять за основу одини габариты
                        self.maxHeightImage = self.maxH_window >= maxH_image ? maxH_image : self.maxH_window;
                        self.maxWidthImage = self.maxW_window >= maxW_image + self.infoWindow ? maxW_image : self.maxW_window - self.infoWindow;

                        //Картинка квадратная
                        //1. Определяем минимальную
                        var baseWH = self.maxHeightImage < self.maxWidthImage ? self.maxHeightImage : self.maxWidthImage;

                        //3. Проверяем ширину экрана
                        if (self.clientWidth < 751) {
                            document.getElementById(self.wrapperId).style.maxHeight = (self.maxW_window * 2) + 'px';
                            document.getElementById(self.wrapperId).style.maxWidth = self.maxW_window + 'px';

                            document.getElementById(self.imageSizeId).style.maxHeight = self.maxW_window + 'px';
                            document.getElementById(self.imageSizeId).style.maxWidth = self.maxW_window + 'px';
                        } else {
                            document.getElementById(self.wrapperId).style.maxHeight = baseWH + 'px';
                            document.getElementById(self.wrapperId).style.maxWidth = (baseWH + self.infoWindow) + 'px';

                            document.getElementById(self.imageSizeId).style.maxHeight = baseWH + 'px';
                            document.getElementById(self.imageSizeId).style.maxWidth = baseWH + 'px';
                        }

                    }
                },
                initEvents: function () {
                    var self = this;
                    $window.onresize = function () {
                        if (self.gallery.open) {
                            self.gallery.initMaxWindow();
                            self.gallery.onAlignHeight();
                        }
                    };
                },
                initFeed: function () {
                    var self = this;
                    self.groupedFeed();
                    self.initEvents();
                    self.loadMore();
                },
                loadMore: function () {

                    var self = this;

                    var lengthData = self.list.dataOriginal.length;
                    if (lengthData != self.list.index) {
                        var count = 0;
                        if (lengthData > self.list.index + self.list.pageCount) {
                            count = self.list.pageCount;
                        } else {
                            count = lengthData - self.list.index;
                        }
                        //Дописать к существующим с группировкой
                        var index = angular.copy(self.list.index);
                        for (var i = 0; i < count; i++) {
                            self.list.dataOriginal[index + i].open = true;
                        }
                        self.list.index += count;

                    }

                },
                init: function () {
                    var self = this;

                    self.getAllData().then(function (data) {
                        self.data = data.data.items;
                        self.initFeed();
                    });

                }

            };
            vm.feed.init();

        }

        function linkFunc(scope, el, attr, ctrl) {


        }
    }


})
(window.angular);
